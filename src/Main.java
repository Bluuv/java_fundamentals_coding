public class Main {

    public static void main(String[] args) {
        System.out.println("Hello Rox");

        // git init - comanda de initializare a unui proiect de git local
        // culoarea rosu = proiecte nebagate pe git
        // verde = fisiere bagate pe git
        // albastru = fisiere adaugate pe git si modificate


        // git add . = comanda de adaugare a fisierelor noi / modificate in git local
        // git commit -m "mesaj" = commit
        // git remote add origin = comanda care face conexiunea cu dintre local si remote (gitlab)
        // git push --set-upstream origin <branch name> = comanda care trimite catre remote (gitlab) ramura principala (main) a proiectului
        // in git avem notiunea de branch (ramura) atunci cand avem un branch nou la primul push trebuie sa adaugam --set-upstream origin <nume branch>

        // git checkout -b <nume branch> = comanda care creeaza o noua ramura (branch)
        /* IMPORTANT! Pentru a putea schimba ramura (branch) inapoi pe main/master, toate modificarile aduse trebuie
        sa fie salvate intr-un commit
        */
        // git checkout <nume branch> = comanda care face switch la branch

        // git pull = comanda care descarca modificarile din remote (gitlab)

    }

}