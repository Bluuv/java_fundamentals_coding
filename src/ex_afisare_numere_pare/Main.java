package ex_afisare_numere_pare;

public class Main {
    public static void main(String[] args) {
        // sa se afiseze toate numerele naturale pare pana la 99

        for (int i = 0; i <= 99; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
        System.out.println("Varianta 2");
        for (int i = 0; i < 99; i += 2) {
            System.out.println(i);
        }
    }
}
