package ex_calculator;

public class Calculator {

    public int sum(int a, int b) {
        return a + b;
    }

    public int scadere(int a, int b) {
        return a - b;
    }

    public int multiply(int a, int b) {
        return a * b;
    }

    public double division(int a, int b) {
        return (double) a / b;
    }
}
