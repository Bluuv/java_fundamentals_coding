package ex_calculator;

import ex_calculator.Calculator;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        readData();
    }

    public static void readData() {
        // sa se creeze un calculator care sa efectueze + - * / pentru 2 nr a si b

        Calculator calc = new Calculator();

        Scanner commandScanner = new Scanner(System.in);

//        System.out.println("Introduceti a: ");
//        int a = commandScanner.nextInt();
//        System.out.println("Introduceti b: ");
//        int b = commandScanner.nextInt();


        String command;
        do {
            System.out.println("Introduceti a: ");
            int a = commandScanner.nextInt();
            System.out.println("Introduceti b: ");
            int b = commandScanner.nextInt();


            System.out.println("+");
            System.out.println("-");
            System.out.println("*");
            System.out.println("/");
            System.out.println("exit");
            System.out.println("Choose action to execute: ");

            command = commandScanner.next();
            int button = 1;

            switch (command) {
                case ("+"):
                    int resultSuma = calc.sum(a, b);
                    System.out.println(resultSuma);
                    break;
                case ("-"):
                    int resultScadere = calc.scadere(a, b);
                    System.out.println(resultScadere);
                    break;
                case ("*"):
                    int resultInmultire = calc.multiply(a, b);
                    System.out.println(resultInmultire);
                    break;
                case ("/"):
                    if (b == 0) {
                        System.out.println("Can not divide by 0");
                    } else {
                        double resultImpartire = calc.division(a, b);
                        System.out.println(resultImpartire);
                    }
                    break;
                case ("exit"):
                    System.out.println("GoodBye");
                    break;
                default:
                    System.out.println("Please provide a valid input");


            }
        } while (!command.equals("exit"));
    }

}


