package ex_manipulare_stringuri;

import java.util.ArrayList;
import java.util.List;

public class DescompunereString {

    public static void main(String[] args) {

        // sa se afiseze nr de vocale , nr de spatii si numarul de semne de punctuatie din textul de mai jos
        // Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        String text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " +
                "when an unknown printer took a galley of type and scrambled it to make a type specimen book. " +
                "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged." +
                " It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, " +
                "and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum";

        char[] arrayVocale = {'a', 'e', 'i', 'o', 'u'};
        int countVocale = 0;
        int countSpatii = 0;
        int countSemnExclamare = 0;

        text = text.toLowerCase();

        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == ' ') {
                countSpatii++;
            } else if (text.charAt(i) == '!') {
                countSemnExclamare++;
            } else {
                for (int j = 0; j < arrayVocale.length; j++) {
                    if (text.charAt(i) == arrayVocale[j]) {
                        countVocale++;
                    }
                }

            }
        }
        System.out.println("Nr vocale: " + countVocale);
        System.out.println("Nr spatii: " + countSpatii);
        System.out.println("Nr semne de exclamare: " + countSemnExclamare);

        String text2 = "ABC, sssa, !?";
        afisareNumarSemnePunctuatie(text2);
    }

    public static void afisareNumarSemnePunctuatie(String text) {

        char[] arrayPunctuatie = {'.', ',', '?', '!'};
        int countSemnPunctuatie = 0;

        for (int i = 0; i < text.length(); i++) {
//            for (int j = 0; j < arrayPunctuatie.length; j++) {
//                if (text.charAt(i) == arrayPunctuatie[j]) {
//                    countSemnPunctuatie++;
//                }
            if(!Character.isLetterOrDigit(text.charAt(i)) && !Character.isWhitespace(text.charAt(i))){
                // sau if(i(Character.isLetterOrDigit(text.charAt(i)) || Character.isWhitespace(text.charAt(i))))
                countSemnPunctuatie++;
            }
        }
        System.out.println(countSemnPunctuatie);
    }
}
