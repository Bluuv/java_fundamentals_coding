package ex_manipulare_stringuri;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        // sa se afiseze toate caracterele unui string in consola
        // string: Ana are mere

        String text = "Ana are mere";

        afisareCaractereDinString(text);
        calculareNumarCuvinteDinString(text);

    }

    public static void afisareCaractereDinString(String text) {
        for (int i = 0; i < text.length(); i++) {
            System.out.println(text.charAt(i));
        }
    }

    public static void calculareNumarCuvinteDinString(String text){
        String[] words = text.split(" ");
        System.out.println(words.length);
        }
    }

