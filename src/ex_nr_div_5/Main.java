package ex_nr_div_5;

public class Main {
    public static void main(String[] args) {

        //sa se calculeze suma numerelor divizibile cu 5 din intervalul 0-50

        int suma = 0;

        for (int i = 0; i <= 50; i++) {
            if (i % 5 == 0) {
                suma = suma + i;
                System.out.println(i);
            }
        }
        System.out.println("suma este: " + suma);
    }
}
