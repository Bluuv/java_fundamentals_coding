package ex_suma_produs;

public class Main {
    public static void main(String[] args) {

        // sa se calculeze suma primelor 5 numere divizibile cu 3 si produsul primelor 4 numere divizibile cu 2

        int i = 0; // 13
        int countDiv3 = 0; // 5
        int countDiv2 = 0; // 4
        int sum = 0; // 30
        int produs = 1; // 384


        while (countDiv3 < 5 || countDiv2 < 4) {
            if (i % 3 == 0 && countDiv3 < 5) {
                sum = sum + i;
                countDiv3++;
            }
            if (i % 2 == 0 && i != 0 && countDiv2 < 4) {
                produs = produs * i;
                countDiv2++;
            }
            i++;
        }
        System.out.println("suma primelor 5 numere divizibile cu 3 este: " + sum);
        System.out.println("produsul primelor 4 numere divizibile cu 2 este: " + produs);

    }
}
